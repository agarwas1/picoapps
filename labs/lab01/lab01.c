#include "pico/stdlib.h"

/**
 * @brief Toggle Subroutine
 *
 * This subroutine toggles the state of a GPIO pin connected to an LED.
 *
 * @param LED_PIN The GPIO pin number connected to the LED.
 * @param LED_DELAY The delay in milliseconds between toggling the LED state.
 */
void toggle_subroutine( uint LED_PIN , uint LED_DELAY){
  gpio_put(LED_PIN, !gpio_get(LED_PIN)); // toggle the state of led pin
  sleep_ms(LED_DELAY); // stop the execution for the specific delay provided 
}


/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever. 
 * 
 * @return int  Application return code (zero for success).
 */
int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    // Do forever...
    while (true) {

      toggle_subroutine(LED_PIN , LED_DELAY);

    }

    // Should never get here due to infinite while-loop.
    return 0;

}
