#include <stdio.h>
#include <stdlib.h>


// Representation is single-precision (float). 
/** * @brief Use the Wallis product algorithm to calculate PI with single precision. 
 * 
 * This function approximates PI value using the Wallis product technique and a single-precision (float) representation.
 * 
 * @param: Num_max_iteration The maximum number of iterations to run. 
 * @return float The PI value was calculated with single precision.
 */

// Single-precision (float) representation
float Single_Precsion_Calculation(int Num_max_iteration) {
    float approx_pi = 1.0f;
    for (int i = 1; i <= Num_max_iteration; i++) {
        float numerator_number = 2.0f * i; // number in the numerator 
        float denominator_number = numerator_number - 1.0f; // number in the denominator 
        approx_pi *= (numerator_number / denominator_number) * (numerator_number / (denominator_number + 2.0f));
    }
    return approx_pi * 2.0f; 
}


// Representation with double precision Calculate PI with double precision by applying the Wallis product procedure. 
/** @brief Use the Wallis product algorithm to calculate PI with double precision. 
 * 
 * This function approximates PI using the Wallis product technique and double-precision representation.
 * 
 * @param: Num_max_iteration The maximum number of iterations to execute. 
 * @return double The PI value was calculated with double precision.
 */

// Double-precision (double) representation
double Double_Precsion_Calculation(int Num_max_iteration) {
   float approx_pi = 1.0;
    for (int i = 1; i <= Num_max_iteration; i++) {
        float numerator_number = 2.0 * i;
        float denominator_number = numerator_number - 1.0;
        approx_pi *= (numerator_number / denominator_number) * (numerator_number / (denominator_number + 2.0));
    }
    return approx_pi * 2.0; 
}

int main() {
   
    int Num_max_iteration = 100000; // maximum number of iterations
    double Pi_value = 3.14159265359; // true value of pi

    // Calculating and printing the value of single-precision PI
    float pi_value_single_precision = Single_Precsion_Calculation(Num_max_iteration);
    printf("Single-precision PI Value: %.15f\n", pi_value_single_precision);
    printf("Approximation error: %.15f\n", fabs(Pi_value - pi_value_single_precision));

    // Calculating and printing the double-precision PI
    double pi_value_double_precision = Double_Precsion_Calculation(Num_max_iteration);
    printf("Double-precision PI Value : %.15lf\n", pi_value_double_precision);
    printf("Approximation error: %.15lf\n", fabs(Pi_value - pi_value_double_precision));

    return 0;
}
